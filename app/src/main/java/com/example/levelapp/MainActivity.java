package com.example.levelapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    SensorManager manager;
    Sensor accel;
    ImageView v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.manager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        this.accel = this.manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.manager.registerListener(this, this.accel, SensorManager.SENSOR_DELAY_UI);

        this.v = findViewById(R.id.acceleration_view);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        layout_constraintHorizontal_bias

        double x_val =  - Math.min(Math.abs(event.values[0]), 10.0);
        if (event.values[0] < 0.0) {
            x_val = -x_val;
        }

        double y_val = Math.min(Math.abs(event.values[1]), 10.0);
        if (event.values[1] < 0.0) {
            y_val = -y_val;
        }

        Log.i("t", x_val + " " + y_val);


        double horizontal_bias = x_val / 20.0 + 0.5;
        double vertical_bias = y_val / 20.0 + 0.5;

        ConstraintLayout cl = (ConstraintLayout) findViewById(R.id.activity_constraint);
        ConstraintSet cs = new ConstraintSet();
        cs.clone(cl);
        cs.setHorizontalBias(R.id.acceleration_view, (float) horizontal_bias);
        cs.setVerticalBias(R.id.acceleration_view, (float) vertical_bias);
        cs.applyTo(cl);

        String s = String.format("x:%f y:%f z:%f\n",
                event.values[0],
                event.values[1],
                event.values[2]);
        Log.e("tag1", s);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.e("accuracy", "accuracy changed");
    }
}
